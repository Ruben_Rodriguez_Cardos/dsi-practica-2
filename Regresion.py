import pandas as pd
from sklearn import preprocessing, cross_validation
from sklearn.decomposition import PCA
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import collections
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from random import random

def seeCasesVsWeek(data,title):
    temp1 = data['total_cases']
    temp2 = data['weekofyear']
    temp3 = {}
    for aux1, aux2 in zip(temp1,temp2):
        if aux2 in temp3.keys():
            temp3[aux2]=temp3[aux2]+aux1
        else:
            temp3[aux2] = aux1

    temp3 = collections.OrderedDict(sorted(temp3.items()))
    plt.plot(temp3.keys(),temp3.values())
    plt.ylabel('Numero de casos')
    plt.xlabel('Semana del mes')
    plt.title(title)
    plt.savefig(title)
    plt.show()

def doRegression(data, data_test, title, rf):

    #Selecciono las features con mas peso
    features = list(rf['Attributes'].head(5))
    
    #Validación cruzada Lineal
    years_train = []
    years_test = []
    
    for i in range(1990,2007):
        years_train.append(i)
    
    for i in range(2007,2011):
        years_test.append(i)

    #Separacion en train/test, 80/20
    #Se debe separar en train/test en funcion del tiempo, es decir
    #Train tendra años anteriores y test años postertiores
    #Rango de años 1990 - 2010 
    # 16 años train, 4 años a test , 80/20
    #Train años 1990 -2006
    #Test años 2007 -2010
    X_train = data.loc[data['year'].isin(years_train)]
    X_test = data.loc[data['year'].isin(years_test)]
    
    Y_train = data.loc[data['year'].isin(years_train)]
    Y_test = data.loc[data['year'].isin(years_test)]

    Y_train = Y_train['total_cases']
    Y_test = Y_test['total_cases']

    scores=[]

    for i in range(1,11):
        for weights in ['uniform', 'distance']:
            knn = KNeighborsRegressor(n_neighbors=i,weights=weights)
            knn.fit(X_train, Y_train)
            score = knn.score(X_test, Y_test)
            scores.append(score)

    #Obtengo el mejor resultado y su posicion
    pos = 0
    max = -2

    for i in range(0,len(scores)-1):
        if scores[i] > max:
            max=scores[i]
            pos=i

    weight = 'distance'
    K_value = 0
    if pos%2 !=0:
        weight = 'uniform'
    K_value = int(pos/2)+1

    #Aqui X_Test debe usar los valores correspondiente
    X_train = X_train[features]
    X_test = data_test[features]

    #Regresión
    knn = KNeighborsRegressor(n_neighbors=K_value,weights=weight)
    knn.fit(X_train,Y_train)
    
    Y_test=knn.predict(X_test)
    file = open(title+'.txt',"w")
    file.write(str(Y_test)) 
    file.close()

    plt.plot(Y_train.tolist(),'--', label= "Casos")
    plt.plot(Y_test,'-', label = "Predicción")
    plt.ylabel('Numero de casos')
    plt.legend(loc="upper left") 
    plt.title(title)
    plt.savefig(title)
    plt.show()

    return Y_test

def doRandomForest(data,title): 
    
    # Features
    features= ('weekofyear', 'ndvi_ne', 'ndvi_nw', 'ndvi_se', 'ndvi_sw',
       'precipitation_amt_mm', 'reanalysis_air_temp_k',
       'reanalysis_avg_temp_k', 'reanalysis_dew_point_temp_k',
       'reanalysis_max_air_temp_k', 'reanalysis_min_air_temp_k',
       'reanalysis_precip_amt_kg_per_m2',
       'reanalysis_relative_humidity_percent', 'reanalysis_sat_precip_amt_mm',
       'reanalysis_specific_humidity_g_per_kg', 'reanalysis_tdtr_k',
       'station_avg_temp_c', 'station_diur_temp_rng_c', 'station_max_temp_c',
       'station_min_temp_c', 'station_precip_mm')

    X = data[[ 'weekofyear', 'ndvi_ne', 'ndvi_nw', 'ndvi_se', 'ndvi_sw',
       'precipitation_amt_mm', 'reanalysis_air_temp_k',
       'reanalysis_avg_temp_k', 'reanalysis_dew_point_temp_k',
       'reanalysis_max_air_temp_k', 'reanalysis_min_air_temp_k',
       'reanalysis_precip_amt_kg_per_m2',
       'reanalysis_relative_humidity_percent', 'reanalysis_sat_precip_amt_mm',
       'reanalysis_specific_humidity_g_per_kg', 'reanalysis_tdtr_k',
       'station_avg_temp_c', 'station_diur_temp_rng_c', 'station_max_temp_c',
       'station_min_temp_c', 'station_precip_mm']]
    
    y = data['total_cases']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)
    regressor = RandomForestRegressor(n_estimators= 10, criterion='mae', random_state=0)
    regressor.fit(X_train, y_train)
    y_pred = regressor.predict(X_test)
    mae = mean_absolute_error(y_test,y_pred)
    print("\nError Measure ", mae)
    # FEATURE RELEVANCIES
    print ('Feature Relevances')
    rf=pd.DataFrame({'Attributes': features , 'Random Forests':regressor.feature_importances_})
    rf=rf.sort_values(by='Random Forests', ascending=0)
    title=title+".csv"
    rf.to_csv(title, index=False)
    return rf

def createSubmitFile(casos_sj,casos_iq):
    data = pd.read_csv('Data/submission_format.csv')
    
    #Uno los datos, primero san juan y luego iq
    casos_sj=casos_sj.tolist()
    casos_iq=casos_iq.tolist()
    aux=casos_sj+casos_iq
    casos=[]
    for i in aux:
        casos.append(int(i))
    data['total_cases']=casos
    data.to_csv('Data/submission_format_ready.csv', index=False)
    
def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        if feature_name != 'year' and feature_name != 'weekofyear' and feature_name != 'total_cases':
            max_value = df[feature_name].max()
            min_value = df[feature_name].min()
            result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result
    

def main():

    #Carga de los datos
    data = pd.read_csv('Data/dengue_features_train.csv')
    #print(data.columns)
    aux = pd.read_csv('Data/dengue_labels_train.csv')
    aux = aux['total_cases'].values.tolist()

    #Union de los datos
    data['total_cases'] = aux

    #Separo los datos por ciudad
    data_sj = data.loc[data['city'] == 'sj']
    data_iq = data.loc[data['city'] == 'iq']

    #Eliminación de columnas innecesarias, redundatos o erroneas
    colsToDrop=['city','week_start_date']
    data_sj=data_sj.drop(columns=colsToDrop)
    data_iq=data_iq.drop(columns=colsToDrop)

    
    #Elimina valores nulos
    data_iq.replace(["NaN", 'NaT'], np.nan, inplace = True)
    data_iq = data_iq.dropna()

    data_sj.replace(["NaN", 'NaT'], np.nan, inplace = True)
    data_sj = data_sj.dropna()

    #Estudio previo
    #seeCasesVsWeek(data_iq,'Ciudad Iq')
    #seeCasesVsWeek(data_sj,'Ciudad San Juan')
  
    #CARGA DE LOS DATOS DE TEST
    #Carga de los datos
    data = pd.read_csv('Data/dengue_features_test.csv')


    #Separo los datos por ciudad
    data_test_sj = data.loc[data['city'] == 'sj']
    data_test_iq = data.loc[data['city'] == 'iq']

    #Eliminación de columnas innecesarias, redundatos o erroneas
    colsToDrop=['city']
    data_test_sj=data_test_sj.drop(columns=colsToDrop)
    data_test_iq=data_test_iq.drop(columns=colsToDrop)

    #NO SE PUEDEN ELIMINAR LOS REGISTROS ERRONEOS O IMCOMPLETOS DE LOS TEST
    #¿SUSTITUIR/ASUMIR VALORES POR DEFECTO?

    #Elimina valores nulos NO SUSTITUYE?
    data_test_iq.replace(["NaN", 'NaT'], 0, inplace = True)
    data_test_iq.fillna(0, inplace=True)
    #data_test_iq = data_test_iq.dropna()

    data_test_sj.replace(["NaN", 'NaT'], 0, inplace = True)
    data_test_sj.fillna(0, inplace=True)
    #data_test_sj = data_test_sj.dropna()

    #Normalización de los datos
    #NO DEBEN NORMALIZARSE TODAS LAS COLUMNAS
    #AÑO,SEMANA DEL MES,CASOS TOTALES NO DEBEN NORMALIZARSE
    data_sj = normalize(data_sj)
    data_iq = normalize(data_iq)

    #Prueba Random Forest
    rf_sj = doRandomForest(data_sj,"Random Forest San Juan")
    rf_iq = doRandomForest(data_iq,"Random Forest IQ")

    #KNN
    casos_sj = doRegression(data_sj,data_test_sj,'Regresion Ejemplo San juan',rf_sj)
    casos_iq = doRegression(data_iq,data_test_iq,'Regresion Ejemplo IQ',rf_iq)

    #Añado los resultados al submit file
    createSubmitFile(casos_sj,casos_iq)
    
    


if __name__ == "__main__":
    main()